import 'dart:io';

import 'package:get/get_connect/http/src/response/response.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sonocare_partner2/data/api/api_client.dart';
import 'package:sonocare_partner2/util/app_constants.dart';
import 'package:http/http.dart' as http;

class DeliveryRepo {
  ApiClient apiClient;
  final SharedPreferences sharedPreferences;
  DeliveryRepo({required this.sharedPreferences, required this.apiClient});

  Future<Response> getDeliveryRequests() async {
    return await apiClient.getData(AppConstants.GET_DELIVERY_REQUESTS, 'Hospital');
  }
  Future<Response> rejectDeliveryRequest({required String requestID}) async {
    return await apiClient.postData(AppConstants.REJECT_DELIVERY_REQUESTS, 'Hospital', {'requestID':requestID});
  }
  Future<Response> acceptDeliveryRequest({required String requestID}) async {
    return await apiClient.postData(AppConstants.ACCEPT_DELIVERY_REQUESTS, 'Hospital', {'requestID':requestID});
  }
}
