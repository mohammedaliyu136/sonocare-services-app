import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sonocare_partner2/data/api/api_client.dart';
import 'package:sonocare_partner2/data/model/response/pharmacy_model.dart';
import 'package:sonocare_partner2/util/app_constants.dart';
import 'package:http/http.dart' as http;


class PharmacyRepo {
  ApiClient apiClient;
  final SharedPreferences sharedPreferences;
  PharmacyRepo({required this.sharedPreferences, required this.apiClient});


  Future<Response> getProductsList() async {
    return await apiClient.getData(AppConstants.GET_PRODUCTS_LIST, '');
  }

  Future<Response> addProduct(PharmacyProductModel product) async {
    return await apiClient.postData(AppConstants.POST_NEW_PRODUCT, '', product.toJson());
  }

  Future<Response> updateProduct(PharmacyProductModel product) async {
    return await apiClient.postData(AppConstants.POST_UPDATE_PRODUCT, '', product.toJson());
  }

  Future<Response> getProductsRequests() async {
    return await apiClient.getData(AppConstants.GET_PRODUCT_REQUESTS, '');
  }

}
