import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sonocare_partner2/data/api/api_client.dart';
import 'package:sonocare_partner2/data/model/response/hospital_model.dart';
import 'package:sonocare_partner2/util/app_constants.dart';
import 'package:http/http.dart' as http;


class HospitalRepo {
  ApiClient apiClient;
  final SharedPreferences sharedPreferences;
  HospitalRepo({required this.sharedPreferences, required this.apiClient});

  //hospitalRequests
  Future<Response> getHospitalRequestsList() async {
    return await apiClient.getData(AppConstants.Get_Hospital_Requests_List, 'Hospital');
  }//hospitalRequests
  Future<Response> getHospitalServicesList() async {
    return await apiClient.getData(AppConstants.Get_Hospital_Services_List, 'Hospital');
  }
  Future<Response> addHospitalService({required HospitalServiceModel hospitalService}) async {
    return await apiClient.postData(AppConstants.Add_Hospital_Service, 'Hospital', hospitalService.toJson());
  }
  Future<Response> updateHospitalService({required HospitalServiceModel hospitalService}) async {
    return await apiClient.postData(AppConstants.Update_Hospital_Service, 'Hospital', hospitalService.toJson());
  }
  Future<Response> deleteHospitalService({required HospitalServiceModel hospitalService}) async {
    return await apiClient.postData(AppConstants.Delete_Hospital_Service, 'Hospital', hospitalService.toJson());
  }
  Future<Response> acceptHospitalAppointment({required String appointmentID}) async {
    return await apiClient.postData(AppConstants.Accept_Hospital_appointment, 'Hospital', {'appointment-id':appointmentID});
  }
  Future<Response> cancelHospitalAppointment({required String appointmentID}) async {
    return await apiClient.postData(AppConstants.Cancel_Hospital_appointment, 'Hospital', {'appointment-id':appointmentID});
  }
}
