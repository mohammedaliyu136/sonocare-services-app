import 'dart:io';

import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sonocare_partner2/data/api/api_client.dart';
import 'package:sonocare_partner2/util/app_constants.dart';
import 'package:http/http.dart' as http;

class AmbulanceRepo {
  ApiClient apiClient;
  final SharedPreferences sharedPreferences;
  AmbulanceRepo({required this.sharedPreferences, required this.apiClient});

  Future<Response> getAmbulanceRequests() async {
    return await apiClient.getData(AppConstants.GET_AMBULANCE_REQUESTS, 'Hospital');
  }
  Future<Response> rejectAmbulanceRequest({required String requestID}) async {
    return await apiClient.postData(AppConstants.REJECT_AMBULANCE_REQUESTS, 'Hospital', {'requestID':requestID});
  }
  Future<Response> acceptAmbulanceRequest({required String requestID}) async {
    return await apiClient.postData(AppConstants.ACCEPT_AMBULANCE_REQUESTS, 'Hospital', {'requestID':requestID});
  }
}
