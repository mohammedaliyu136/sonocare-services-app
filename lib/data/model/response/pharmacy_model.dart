class PharmacyRequestsModel{
  late String? id;
  late String image;
  late String name;
  late String phone;
  late String productName;
  late String status;
  late String qty;
  late String destination;
  late String createdAT;
  late String date = '26th May 2021';
  late String time = '12:12pm';
  late String deliveryType = 'Self pick up';
  late PatientProfileModel patientProfile;
  late List<PharmacyProductModel> products = [];
  PharmacyRequestsModel({this.id, required this.name, required this.image, required this.products, required this.patientProfile});

  PharmacyRequestsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['patient_name'];
    phone = json['patient_phone'];
    productName = json['product_name'];
    destination = json['to_location']??'';
    status = json['status'];
    qty = json['qty'];
    date = json['created_at'].split(' ')[0];
    time = json['created_at'].split(' ')[1];
    createdAT = json['created_at'];
    List<PharmacyProductModel> products = [];
    DrugTypeModel drugType1 = DrugTypeModel(id: '', name: 'Tablet');
    DrugTypeModel drugType2 = DrugTypeModel(id: '', name: 'Syrup');
    DrugTypeModel drugType3 = DrugTypeModel(id: '', name: 'Capsule');
    DrugTypeModel drugType4 = DrugTypeModel(id: '', name: 'Injection');

    DrugBrandModel drugBrand1 = DrugBrandModel(id: '', name: 'Emzo');
    DrugBrandModel drugBrand2 = DrugBrandModel(id: '', name: 'J&J');
    DrugBrandModel drugBrand3 = DrugBrandModel(id: '', name: 'Drug brand');
    products.add(PharmacyProductModel(name: 'Vitamin C', id: '', unitPrice: 200, type: drugType1, packPacketPrice: 2000, brand: drugBrand1, dosage: '50/100', description: 'Acetaminophen is used to treat mild to moderate and pain, to treat moderate to severe pain in conjunction with opiates, or to reduce fever. Common conditions treated include headache, muscle aches, arthritis, backache, toothaches, sore throat, colds, flu, and fevers.'));
    products.add(PharmacyProductModel(name: 'Eye Drop',  id: '', unitPrice: 200, type: drugType2, packPacketPrice: 2000, brand: drugBrand3, dosage: '50/100', description: 'Acetaminophen is used to treat mild to moderate and pain, to treat moderate to severe pain in conjunction with opiates, or to reduce fever. Common conditions treated include headache, muscle aches, arthritis, backache, toothaches, sore throat, colds, flu, and fevers.'));
    products.add(PharmacyProductModel(name: 'Multivitamins',  id: '', unitPrice: 200, type: drugType3, packPacketPrice: 2000, brand: drugBrand2, dosage: '50/100', description: ''));
    products.add(PharmacyProductModel(name: 'Inhaler',  id: '', unitPrice: 200, type: drugType1, packPacketPrice: 2000, brand: drugBrand1, dosage: '50/100', description: ''));
    products.add(PharmacyProductModel(name: 'Ampiclox',  id: '', unitPrice: 200, type: drugType2, packPacketPrice: 2000, brand: drugBrand1, dosage: '50/100', description: ''));
    products.add(PharmacyProductModel(name: 'Postinor',  id: '', unitPrice: 200, type: drugType3, packPacketPrice: 2000, brand: drugBrand1, dosage: '50/100', description: ''));
    products.add(PharmacyProductModel(name: 'Paracetamol',  id: '', unitPrice: 200, type: drugType4, packPacketPrice: 2000, brand: drugBrand1, dosage: '50/100', description: ''));

    this.products = products;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    return data;
  }

}

class PatientProfileModel{
  String id;
  String name;
  PatientProfileModel({required this.id, required this.name});
}
class PharmacyProductModel{
  String? id;
  late String name;
  late String description;
  late DrugCategoryModel? category;
  late double unitPrice;
  late double packPacketPrice;
  late String dosage;
  late DrugBrandModel brand;
  late DrugTypeModel type;
  PharmacyProductModel({
    this.id, required this.name, required this.description, this.category, required this.type,
    required this.brand, required this.unitPrice, required this.packPacketPrice, required this.dosage});

  /*
  name": "Test drug",
            "amount": "200",
            "dosage": "2/1",
   */
  PharmacyProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    unitPrice = double.parse(json['amount'].toString());
    packPacketPrice = double.parse(json['amount'].toString());
    dosage = json['dosage'].toString();
    description = '';

    DrugTypeModel drugType1 = DrugTypeModel(id: '', name: 'Tablet');
    DrugBrandModel drugBrand1 = DrugBrandModel(id: '', name: 'Emzo');
    DrugCategoryModel drugCategory = DrugCategoryModel(id: '1', name: 'pharm cat 1');
    category = drugCategory;
    brand = drugBrand1;
    type = drugType1;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['amount'] = unitPrice;
    data['dosage'] = dosage;
    return data;
  }
}
class DrugCategoryModel{
  String id;
  String name;
  DrugCategoryModel({required this.id, required this.name});
}
class DrugTypeModel{
  String id;
  String name;
  DrugTypeModel({required this.id, required this.name});
}
class DrugBrandModel{
  String id;
  String name;
  DrugBrandModel({required this.id, required this.name});
}