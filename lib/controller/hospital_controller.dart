import 'dart:convert';

import 'package:get/get.dart';
import 'package:sonocare_partner2/data/model/response/hospital_model.dart';
import 'package:sonocare_partner2/data/model/response/response_model.dart';
import 'package:sonocare_partner2/data/repository/hospital_repo.dart';

class HospitalController extends GetxController implements GetxService {
  final HospitalRepo hospitalRepo;
  HospitalController({required this.hospitalRepo});

  List<HospitalRequestsModel> hospitalRequests = [];
  List<HospitalRequestsModel> hospitalRequestsOld = [];
  List<HospitalServiceModel> hospitalServices= [];
  List<HospitalServiceModel> hospitalServicesOld = [];

  bool loadingHospital = false;

  Future<void> searchHospitalRequests({searchText})async {
    if(hospitalRequestsOld.isEmpty || hospitalRequestsOld.length==hospitalRequests.length){
      hospitalRequestsOld = hospitalRequests;
    }else{
      hospitalRequests = hospitalRequestsOld;
    }
    hospitalRequests = [];
    for( var i = 0 ; i < hospitalRequestsOld.length; i++ ) {
      if(hospitalRequestsOld[i].name.isCaseInsensitiveContainsAny(searchText)){
        hospitalRequests.add(hospitalRequestsOld[i]);
      }
    }
    update();
  }
  Future<void> searchHospitalServices({searchText})async {
    if(hospitalServicesOld.isEmpty || hospitalServicesOld.length==hospitalServices.length){
      hospitalServicesOld = hospitalServices;
    }else{
      hospitalServices = hospitalServicesOld;
    }
    hospitalServices = [];
    for( var i = 0 ; i < hospitalServicesOld.length; i++ ) {
      if(hospitalServicesOld[i].name.isCaseInsensitiveContainsAny(searchText)){
        hospitalServices.add(hospitalServicesOld[i]);
      }
    }
    update();
  }

  Future<void> getHospitalRequests() async {
    hospitalRequests = [];
    loadingHospital = true;

    /// /hospital/appointments

    Response response = await hospitalRepo.getHospitalRequestsList();

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      print('------');
      //print(response.body.runtimeType);
      var appointments = response.body['appointments'];
      for( var i = 0 ; i < appointments.length; i++ ) {
        hospitalRequests.add(HospitalRequestsModel.fromJson(appointments[i]));
      }
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }

    await Future.delayed(const Duration(seconds: 3));
    /*
    for( var i = 0 ; i < 10; i++ ) {
      List<HospitalServiceModel> Services = [];
      for( var t = 0 ; t < 5; t++ ) {
        Services.add(HospitalServiceModel(name: 'Malaria medication $t'));
      }
      hospitalRequests.add(HospitalRequestsModel(id: '$i', name: 'pharm cat $i', Services: Services, image: '', patientProfile: PatientProfileModel(id: '', name: 'Mohammed Aliyu')));
    }*/
    //-------------------
    /*
    List<HospitalServiceModel> services = [];
    services.add(HospitalServiceModel(name: 'GP Consultation', id: '', cost: '10,000'));
    services.add(HospitalServiceModel(name: 'Specialist Consultation',  id: '', cost: '200'));
    services.add(HospitalServiceModel(name: 'Antenatal Care',  id: '', cost: '200'));
    services.add(HospitalServiceModel(name: 'Surgery',  id: '', cost: '200'));
    hospitalRequests.add(HospitalRequestsModel(id: '1', name: 'Abigail Alexandra', services: services, image: '', patientProfile: PatientProfileModel(id: '', name: 'Abigail Alexandra'), detail: 'Ante-natal Care', location: 'Gwarinpa, Abuja'));
    hospitalRequests.add(HospitalRequestsModel(id: '2', name: 'Caroline Carolyn', services: services, image: '', patientProfile: PatientProfileModel(id: '', name: 'Caroline Carolyn'), detail: 'Ante-natal Care', location: 'Gwarinpa, Abuja'));
    hospitalRequests.add(HospitalRequestsModel(id: '3', name: 'Lillian Lily', services: services, image: '', patientProfile: PatientProfileModel(id: '', name: 'Lillian Lily'), detail: 'Ante-natal Care', location: 'Gwarinpa, Abuja'));
    hospitalRequests.add(HospitalRequestsModel(id: '4', name: 'Sonia Sophie', services: services, image: '', patientProfile: PatientProfileModel(id: '', name: 'Sonia Sophie'), detail: 'Ante-natal Care', location: 'Gwarinpa, Abuja'));
    */
    //-------------------
    loadingHospital = false;
    update();
  }

  Future<void> getHospitalServices()async{
    hospitalServices = [];
    loadingHospital = true;
    update();

    Response response = await hospitalRepo.getHospitalServicesList();

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      print('------');
      //print(response.body.runtimeType);
      var services = response.body['service'];
      for( var i = 0 ; i < services.length; i++ ) {
        hospitalServices.add(HospitalServiceModel.fromJson(services[i]));
      }
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }

    //hospitalServices.add(HospitalServiceModel(name: 'GP Consultation', id: '', cost: '10,000'));
    //hospitalServices.add(HospitalServiceModel(name: 'Specialist Consultation',  id: '', cost: '21,000'));
    //hospitalServices.add(HospitalServiceModel(name: 'Antenatal Care',  id: '', cost: '12,000'));
    //hospitalServices.add(HospitalServiceModel(name: 'Surgery',  id: '', cost: '200,000'));
    loadingHospital = false;
    update();
  }
  Future<bool> addHospitalService({required HospitalServiceModel hospitalService}) async {
    loadingHospital = true;
    update();
    //await Future.delayed(const Duration(seconds: 3));
    //hospitalServices.add(hospitalService);
    //hospitalServicesOld = [];
    //loadingHospital = false;

    //update();

    Response response = await hospitalRepo.addHospitalService(hospitalService:hospitalService);

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      hospitalServices.add(hospitalService);
      hospitalServicesOld = [];
      loadingHospital = false;
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }
    update();
    return true;
  }
  Future<bool> updateHospitalService({required HospitalServiceModel hospitalService, required int index})async{
    loadingHospital = true;
    update();
    Response response = await hospitalRepo.updateHospitalService(hospitalService:hospitalService);

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      hospitalServices[index].name = hospitalService.name;
      hospitalServices[index].cost = hospitalService.cost;
      loadingHospital = false;
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }
    update();
    return true;
  }
  deleteHospitalService({required HospitalServiceModel hospitalService})async{
    loadingHospital = true;
    update();
    Response response = await hospitalRepo.deleteHospitalService(hospitalService:hospitalService);

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      hospitalServices.remove(hospitalService);
      loadingHospital = false;
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }
    update();
    return true;
  }
  Future<ResponseModel> acceptHospitalAppointment({required String appointmentID})async{
    loadingHospital = true;
    update();
    Response response = await hospitalRepo.acceptHospitalAppointment(appointmentID:appointmentID);

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      loadingHospital = false;
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }
    update();
    return responseModel;
  }
  Future<ResponseModel> cancelHospitalAppointment({required String appointmentID})async{
    loadingHospital = true;
    update();
    Response response = await hospitalRepo.cancelHospitalAppointment(appointmentID:appointmentID);

    ResponseModel responseModel;
    if(response.statusCode == 200) {
      responseModel = ResponseModel(true, '');
      loadingHospital = false;
      update();
    } else {
      responseModel = ResponseModel(false, '');
    }
    update();
    return responseModel;
  }
}
